# README #

##### Version:   1.0.0 #####
##### Author:    Ionut Buciuman #####

## Prerequisites ##

[Node](https://nodejs.org/en/)

## Commands ##
    
    > node convert --conversionType=[value] --inputFilename=[value] --outputFilename=[value]
    
    > node convert -t=[value] -i=[value] -o=[value]


## Parameters ## 

| Parameter             | Shorthand     | Sample Values             |
| ----------------------|:-------------:|---------------------------| 
| --conversionType=     |   -t=         | json / html               |
| --inputFilename=      |   -i=         | "input.json"              |
| --outputFilename=     |   -o=         | "output.html"             |


## Example ##
    
    > node convert -t=json -i=input.html -o=output.json 