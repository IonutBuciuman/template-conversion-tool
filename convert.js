'use strict';

const fs = require('fs');

let inputFileContent = '';
let outputFileContent = '';

let args = extractArgs(process.argv);
console.info('arguments: ', JSON.stringify(args, null, 4));

checkArgs(args);
inputFileContent = fs.readFileSync(args.inputFileName).toString();

switch (args.conversionType) {
	case 'json': outputFileContent = convertJSON2HTML(inputFileContent); break;
	case 'html': outputFileContent = convertHTML2JSON(inputFileContent); break;
	default: console.info('something is missing...\n', process.argv); break;
}

fs.writeFileSync(args.outputFileName, outputFileContent);

// - - utilities - -
function convertHTML2JSON(content) {
	const result = JSON.stringify({ template: content });
	console.info('\nJSON template: \n', result);
	return result;
}

function convertJSON2HTML(content) {
	let result = JSON.parse(content);
	
	if (Array.isArray(result)) {
		let tpls = '';
		
		for (let i = 0; i < result.length; i++) {
			tpls += getFormattedTemplate(result[i]);
		}
		result = tpls;
	} else {
		result = getFormattedTemplate(result);
	}
	
	console.info('\nHTML template: \n', result);
	return result;
}

function extractArgs(argv) {
	let args = {
		conversionType: '',
		inputFileName: '',
		outputFileName: ''
	};
	
	for (let index = 2; index < argv.length; index++) {
		const argument = argv[index];
		
		if (/^--conversionType=/.test(argument) || /^-t=/.test(argument)) {
			args.conversionType = argument.split('=')[1].toLowerCase();
		}
		
		if (/^--inputFile=/.test(argument) || /^-i=/.test(argument)) {
			args.inputFileName = argument.split('=')[1];
		}
		
		if (/^--outputFile=/.test(argument) || /^-o=/.test(argument)) {
			args.outputFileName = argument.split('=')[1];
		}
	}
	
	return args;
}

function checkArgs(args) {
	let isOk = true;
	
	console.info('... checking arguments.. ');
	
	Object.keys(args).forEach(function (key) {
		let value = args[key];
		
		if ((!value && value !== 0) || value === "") {
			isOk = false;
			console.error('\n......... [%s] is missing..', key);
		}
	});
	
	if (!isOk) {
		showHelp();
		console.info('\n\n..exited with code 1\n');
		process.exit(1);
	}
}

function showHelp() {
	const helpOptions = [
		{
			description: 'this argument specifies the conversion type [required]',
			command: '--conversionType=',
			shorthand: '-t='
		},
		{
			description: 'this argument specifies the name of the input file (extension included) [required]',
			command: '--inputFilename=',
			shorthand: '-i='
		},
		{
			description: 'this argument specifies the name of the output file (extension included) [required]',
			command: '--outputFilename=',
			shorthand: '-o='
		},
	];
	
	console.info('\n----------------------------------------------- HELP -----------------------------------------------\n');

	helpOptions.forEach(function (option, index) {
		console.info('\t %s \t %s \t : %s', option.command, option.shorthand, option.description);
	});
}

function getFormattedTemplate(item) {
	let tpl = '';
	
	tpl += '/************************************************************************************************/\n';
	tpl += '\n';
	tpl += item.name || 'no template "name" specified';
	tpl += '\n';
	tpl += '\n';
	tpl += '/************************************************************************************************/\n';
	tpl += '\n';
	tpl += '\n';
	tpl += item.template  || 'no "template" specified';
	tpl += '\n';
	tpl += '\n';
	tpl += '\n';
	tpl += '\n';

	return tpl;
}